package simpledb;

import java.util.*;

/**
 * Filter is an operator that implements a relational select.
 */
public class Filter extends Operator {

    private static final long serialVersionUID = 1L;
    
    private OpIterator childOpIter;
    private Predicate predicate;
    private TupleDesc tupleDesc;

    /**
     * Constructor accepts a predicate to apply and a child operator to read
     * tuples to filter from.
     * 
     * @param p
     *            The predicate to filter tuples with
     * @param child
     *            The child operator
     */
    public Filter(Predicate p, OpIterator child) {
    	this.predicate = p;
        this.childOpIter = child;
        this.tupleDesc = child.getTupleDesc();
    }

    public Predicate getPredicate() {
        return this.predicate;
    }

    public TupleDesc getTupleDesc() {
        return this.tupleDesc;
    }

    public void open() throws DbException, NoSuchElementException,
            TransactionAbortedException {
        childOpIter.open();
        super.open();
    }

    public void close() {
    	super.close();
        childOpIter.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        childOpIter.rewind();
    }

    /**
     * AbstractDbIterator.readNext implementation. Iterates over tuples from the
     * child operator, applying the predicate to them and returning those that
     * pass the predicate (i.e. for which the Predicate.filter() returns true.)
     * 
     * @return The next tuple that passes the filter, or null if there are no
     *         more tuples
     * @see Predicate#filter
     */
    protected Tuple fetchNext() throws NoSuchElementException,
            TransactionAbortedException, DbException {
        
    	while(childOpIter.hasNext()){
        	Tuple tupleToFilter = childOpIter.next();
        	
        	if(predicate.filter(tupleToFilter)){
                return tupleToFilter;		        		
        	}
        }
    	return null;
    }

    @Override
    public OpIterator[] getChildren() {
        return new OpIterator[] {this.childOpIter};
    }

    @Override
    public void setChildren(OpIterator[] children) {
    	if (this.childOpIter!=children[0])
    	{
    	    this.childOpIter = children[0];
    	}        
    }
}
