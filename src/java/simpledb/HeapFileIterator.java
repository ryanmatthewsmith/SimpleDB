package simpledb;
import java.io.*;
import java.util.*;

/**
 *  HeapFile iterator
 */
public class HeapFileIterator implements DbFileIterator{
	
	private final HeapFile hFile;
	private final TransactionId tid;
	private int currentPage;
	private Iterator<Tuple> currentPageTuples;
	
	/**
	 * constructs HeapFileIterator
	 */
	public HeapFileIterator(HeapFile hFile, TransactionId tid){
		this.hFile = hFile;
		this.tid = tid;
		this.currentPage = -1;
		this.currentPageTuples = null;
	}
	
    /**
     * Opens the iterator
     * @throws DbException when there are problems opening/accessing the database.
     */
    public void open()
        throws DbException, TransactionAbortedException{
    	if (this.currentPage != -1){
    		throw new DbException("DbFile Iterator already open!");
    	}
    	if (this.hFile.numPages() == 0){
    		throw new DbException("Empty DbFile!");
    	}
    	
    	this.currentPage = 0;
        //get first page iterator
    	this.currentPageTuples = nextTuples();
    	
    }

    /** @return true if there are more tuples available, false if no more tuples or iterator isn't open. */
    public boolean hasNext()
        throws DbException, TransactionAbortedException{
    	
    	//if(this.currentPage == -1){
    	//	throw new DbException("must open before calling hasNext");
    	//}
    	if (this.currentPage == -1){
    		return false;
    	}
    	
    	if (this.currentPageTuples.hasNext()){
    		return true;
    	}
    	else{
    		currentPage++;
    		if (this.currentPage >= hFile.numPages()){
    			return false;
    		}
    		this.currentPageTuples = nextTuples();
    		return this.hasNext();
    	}
    }

    /**
     * Gets the next tuple from the operator (typically implementing by reading
     * from a child operator or an access method).
     *
     * @return The next tuple in the iterator.
     * @throws NoSuchElementException if there are no more tuples
     */
    public Tuple next()
        throws DbException, TransactionAbortedException, NoSuchElementException{
    	//if(this.currentPage == -1){
    	//	throw new DbException("must open before calling next");
    	//}  //TODO: ask TA, this seemed appropriate behavior but fails iterator test
    	if (!hasNext()){
    		throw new NoSuchElementException();
    	}
    	return this.currentPageTuples.next();
    }

    /**
     * Resets the iterator to the start.
     * @throws DbException When rewind is unsupported.
     */
    public void rewind() throws DbException, TransactionAbortedException{
    	if(this.currentPage == -1){
    		throw new DbException("must open before calling next");
    	}
     	this.close();
     	this.open();
    }

    /**
     * Closes the iterator.
     */
    public void close(){
    	this.currentPage = -1;
    	this.currentPageTuples = null;
    }
    
    /**
     *  Next tuple iterator
     * @throws DbException 
     * @throws TransactionAbortedException 
     */
    private Iterator<Tuple> nextTuples() throws TransactionAbortedException, DbException{
    	Iterator<Tuple> nextTuples = ((HeapPage)
    			                       Database.getBufferPool()
                                               .getPage(tid, new HeapPageId(hFile.getId(),currentPage), Permissions.READ_ONLY))
                                               .iterator();
    	return nextTuples;
    }
}
