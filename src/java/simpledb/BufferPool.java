package simpledb;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 * 
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    /** Bytes per page, including header. */
    private static final int DEFAULT_PAGE_SIZE = 4096;
    private static int pageSize = DEFAULT_PAGE_SIZE;
    
    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;
    private final int maxPages;
    private final Map<PageId,Page> bufferPool;
    private final Map<PageId, Integer> pidToCount;
    private final Map<TransactionId, Set<Page>> uncommittedFlushed;
    private int getPageCallCount;
    private LockManager lock;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        this.maxPages = numPages;
        this.bufferPool = new ConcurrentHashMap<PageId, Page>();
        this.pidToCount = new ConcurrentHashMap<PageId, Integer>();
        this.uncommittedFlushed = new ConcurrentHashMap<TransactionId, Set<Page>>();
        this.getPageCallCount = 0;
        this.lock = new LockManager();
    }
    
    public static int getPageSize() {
      return pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void setPageSize(int pageSize) {
    	BufferPool.pageSize = pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void resetPageSize() {
    	BufferPool.pageSize = DEFAULT_PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, a page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException{
    	
        boolean wait = !lock.acquire(tid, pid, perm);
        
        while(wait){
        	wait = !lock.acquire(tid, pid, perm);
        }
    	
    	getPageCallCount++;
    	//already in pool, return page
    	if (bufferPool.containsKey(pid)){
    		pidToCount.put(pid, getPageCallCount);
    		return bufferPool.get(pid);
    	}
    	
    	//full pool
    	if (bufferPool.size() >= maxPages){
    	   evictPage();
    	}
    	
    	//add page to buffer and return
    	Page page = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
    	bufferPool.put(pid, page);
    	pidToCount.put(pid, getPageCallCount);

    	return page;
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public void releasePage(TransactionId tid, PageId pid) {
    	lock.release(tid, pid);
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        transactionComplete(tid, true);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId pid) {
        return lock.holdsLock(tid, pid);
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        if (commit){
        	prepareLogCommit(tid);
        }else{
        	for (Page p : bufferPool.values()){
        		if(tid.equals(p.isDirty())){
        			bufferPool.put(p.getId(), p.getBeforeImage());
        			p.markDirty(false, null);
        		}
        	}
        	uncommittedFlushed.remove(tid);
        }
        lock.releaseAll(tid);
    }

    /**
     * Add a tuple to the specified table on behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to and any other 
     * pages that are updated (Lock acquisition is not needed for lab2). 
     * May block if the lock(s) cannot be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        DbFile f = Database.getCatalog().getDatabaseFile(tableId);
        ArrayList<Page> changedPages = f.insertTuple(tid, t);
        
       for(Page p : changedPages){
    	   p.markDirty(true, tid);
    	   bufferPool.put(p.getId(), p);
       }
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from and any
     * other pages that are updated. May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        int tableId = t.getRecordId().getPageId().getTableId();
        DbFile f = Database.getCatalog().getDatabaseFile(tableId);
        ArrayList<Page> changedPages = f.deleteTuple(tid, t);
        
        for(Page p: changedPages){
        	p.markDirty(true, tid);
        	bufferPool.put(p.getId(), p);
        }
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        for (PageId pid : bufferPool.keySet()){
        	flushPage(pid);
        }
    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
        
        Also used by B+ tree files to ensure that deleted pages
        are removed from the cache so they can be reused safely
    */
    public synchronized void discardPage(PageId pid) {
    	if(!bufferPool.containsKey(pid)){
    		return;
    	} else{	    	
            bufferPool.remove(pid);
	        
	        if (pidToCount.remove(pid) == null){
	        	System.err.println("pid not found in pidToCount: discardPage");
	        }
    	}
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized void flushPage(PageId pid) throws IOException {
        Page p = bufferPool.get(pid);
        
        TransactionId tid = p.isDirty();
        if (tid == null){
        	return;
        }
        
        Database.getLogFile().logWrite(tid, p.getBeforeImage(), p);
        Database.getLogFile().force();
        
        DbFile dbF = Database.getCatalog().getDatabaseFile(pid.getTableId());
        dbF.writePage(p);
        p.markDirty(false, null);
        trackUncommittedFlushedPages(tid, p);
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized void flushPages(TransactionId tid) throws IOException {
        for (Page p : bufferPool.values()){
        	if(tid.equals(p.isDirty())){
        		flushPage(p.getId());
        	}
        }
    }
    
    /**
     * prepareLogCommit logs dirtied pages in the log file, saves the pages previous
     * state and flushes the log to disk
     */
    public synchronized void prepareLogCommit(TransactionId tid) throws IOException{
    	LogFile log = Database.getLogFile();
    	
    	for (Page p: bufferPool.values()){
    		if(tid.equals(p.isDirty())){
    			log.logWrite(tid, p.getBeforeImage(), p);
    			log.force();
    			p.setBeforeImage();
    		}
    	}
    	
    	if(uncommittedFlushed.containsKey(tid)){
    		for(Page page : uncommittedFlushed.get(tid)){
    			page.setBeforeImage();    			
    		}
    		uncommittedFlushed.remove(tid);
    	}
    	
    }
    
    /**
     * Tracks pages that have been flushed and are dirty so we can properly save page
     * state on commit
     * 
     * @param tid
     * @param p
     */
    private synchronized void trackUncommittedFlushedPages(TransactionId tid,Page p){
    	if(!uncommittedFlushed.containsKey(tid)){
    		uncommittedFlushed.put(tid, new HashSet<Page>());
    	}
    	uncommittedFlushed.get(tid).add(p);
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized void evictPage() throws DbException {
        PageId lruPagePid = findLruPid();
        
        if (!bufferPool.containsKey(lruPagePid)){
        	System.err.println("failure/bug, bp map doesnt match pidCount map");
        }
        
        try{
            flushPage(lruPagePid);
        } catch (IOException e){
        	System.err.println("failure, flush page IO: " + e.getMessage());
        }
        
        bufferPool.remove(lruPagePid);
        
        if (pidToCount.remove(lruPagePid) == null){
        	System.err.println("failure/bug, bp map doesnt match pidCount map");
        }
    }
    
    private synchronized PageId findLruPid() throws DbException{    	
    	
    	List<Entry<PageId, Integer>> sortValues= new ArrayList<Entry<PageId, Integer>>();
        for(Entry<PageId, Integer> item: pidToCount.entrySet()){
        	sortValues.add(item);
        }
        sortValues.sort((a,b) -> a.getValue().compareTo(b.getValue()));
        
        return sortValues.remove(0).getKey();
    }

}
