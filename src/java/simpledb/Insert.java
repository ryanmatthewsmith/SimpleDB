package simpledb;

import java.io.IOException;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator {

    private static final long serialVersionUID = 1L;
    
    private TransactionId tid;
    private OpIterator childOpIter;
    private int tableID;
    private boolean isOpen;
    private boolean insertsComplete;

    /**
     * Constructor.
     *
     * @param t
     *            The transaction running the insert.
     * @param child
     *            The child operator from which to read tuples to be inserted.
     * @param tableId
     *            The table in which to insert tuples.
     * @throws DbException
     *             if TupleDesc of child differs from table into which we are to
     *             insert.
     */
    public Insert(TransactionId t, OpIterator child, int tableId)
            throws DbException {
        this.tid = t;
        this.childOpIter = child;
        this.tableID = tableId;
        this.isOpen = false;
    }

    public TupleDesc getTupleDesc() {
        return new TupleDesc(new Type[] {Type.INT_TYPE});
    }

    public void open() throws DbException, TransactionAbortedException {
    	if(isOpen){
    		throw new DbException("already open iterator, INSERT");
    	}
        childOpIter.open();
        super.open();
        isOpen = true;
        insertsComplete = false;
    }

    public void close() {
        super.close();
    	childOpIter.close();
    	isOpen = false;
        insertsComplete = false;
    }

    public void rewind() throws DbException, TransactionAbortedException {
        childOpIter.rewind();
        insertsComplete = false;
    }

    /**
     * Inserts tuples read from child into the tableId specified by the
     * constructor. It returns a one field tuple containing the number of
     * inserted records. Inserts should be passed through BufferPool. An
     * instances of BufferPool is available via Database.getBufferPool(). Note
     * that insert DOES NOT need check to see if a particular tuple is a
     * duplicate before inserting it.
     *
     * @return A 1-field tuple containing the number of inserted records, or
     *         null if called more than once.
     * @see Database#getBufferPool
     * @see BufferPool#insertTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
    	if (insertsComplete){
    		return null;
    	}
    	
    	int count = 0;
    	while(childOpIter.hasNext()){
    		try{
    			Database.getBufferPool().insertTuple(tid, tableID, childOpIter.next());
    			count++;
    		}catch(IOException e){
    			System.err.println(e.getMessage());
    		}
        }
    	Tuple insertCountTuple = new Tuple(this.getTupleDesc());
    	insertCountTuple.setField(0, new IntField(count));
    	
    	insertsComplete = true;
    	
    	return insertCountTuple;
    }

    @Override
    public OpIterator[] getChildren() {
        return new OpIterator[] {this.childOpIter};
    }

    @Override
    public void setChildren(OpIterator[] children) {
    	if (this.childOpIter!=children[0])
    	{
    	    this.childOpIter = children[0];
    	}        
    }
}
