package simpledb;

import java.io.IOException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;
    
    private TransactionId tid;
    private OpIterator childOpIter;
    private boolean isOpen;
    private boolean deletesComplete;
    
    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     * 
     * @param t
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
    public Delete(TransactionId t, OpIterator child) {
        this.tid = t;
        this.childOpIter = child;
        this.isOpen = false;
    }

    public TupleDesc getTupleDesc() {
    	return new TupleDesc(new Type[] {Type.INT_TYPE});
    }

    public void open() throws DbException, TransactionAbortedException {
    	if(isOpen){
    		throw new DbException("Can't open an already open iterator, Delete");
    	}    	
        childOpIter.open();
        super.open();
        deletesComplete = false;
    }

    public void close() {
        super.close();
        childOpIter.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        childOpIter.rewind();
        deletesComplete = false;
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     * 
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
    	
    	if (deletesComplete){
    		return null;
    	}
    	
    	int count = 0;
    	while(childOpIter.hasNext()){
    		try{
    			Database.getBufferPool().deleteTuple(tid, childOpIter.next());
    			count++;
    		}catch(IOException e){
    			System.err.println(e.getMessage());
    		}
        }
    	Tuple deleteCountTuple = new Tuple(this.getTupleDesc());
    	deleteCountTuple.setField(0, new IntField(count));
    	
    	deletesComplete = true;
    	
    	return deleteCountTuple;
    }

    @Override
    public OpIterator[] getChildren() {
        return new OpIterator[] {this.childOpIter};
    }

    @Override
    public void setChildren(OpIterator[] children) {
    	if (this.childOpIter!=children[0])
    	{
    	    this.childOpIter = children[0];
    	}        
    }
}
