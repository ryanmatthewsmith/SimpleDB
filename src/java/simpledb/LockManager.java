package simpledb;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class LockManager {
	private Map<PageId, LockInfo> locks;
	private Map<TransactionId, Set<TransactionId>> waitsForGraph;
	
	public LockManager(){
		this.locks = new ConcurrentHashMap<PageId, LockInfo>();
		this.waitsForGraph = new ConcurrentHashMap<TransactionId, Set<TransactionId>>();
	}
	
	public boolean acquire(TransactionId tid, PageId pid, Permissions perm) throws TransactionAbortedException{
		synchronized(this.locks){
		if (perm.equals(Permissions.READ_WRITE)){

				if (hasExclusive(tid, pid)){
					waitsForGraph.remove(tid);
					return true;
				}
		    	
				LockInfo newInfo = new LockInfo();
				if(!locks.containsKey(pid)){
					newInfo.tids.add(tid);
					newInfo.isExclusive = true;
					locks.put(pid, newInfo);
					waitsForGraph.remove(tid);
					return true;
				}
					
				LockInfo info2 = locks.get(pid);
					
				//UPGRADE LOCK
				if (!info2.isExclusive && info2.tids.size() == 1 && info2.tids.contains(tid)){
					info2.isExclusive = true;
					locks.put(pid, info2);
					waitsForGraph.remove(tid);
					return true;
				}
					
				if (info2.tids.size() != 0){
					waitsForGraph.put(tid, info2.tids);
					if(detectDeadlock(tid)){
						this.waitsForGraph.remove(tid);
						throw new TransactionAbortedException();
					}
					return false;
				} 					

				newInfo.isExclusive = true;
				newInfo.tids.add(tid);
				locks.put(pid, newInfo);
				waitsForGraph.remove(tid);
				return true;
			}
		//}
		//READ ONLY, SHARED LOCK
		else{
			//synchronized(this.locks){
			    if(holdsLock(tid, pid)){
			    	waitsForGraph.remove(tid);
			    	return true;
			    }
				
			    if(!locks.containsKey(pid)){
					LockInfo info = new LockInfo();
					info.tids.add(tid);
					locks.put(pid, info);
					waitsForGraph.remove(tid);
					return true;
				}
					
				LockInfo info = locks.get(pid);
					
				if (info.isExclusive){
					waitsForGraph.put(tid, info.tids);
					if(detectDeadlock(tid)){
						this.waitsForGraph.remove(tid);
						throw new TransactionAbortedException();
					}
					return false;
				} 
					
				//add lock to shared list and return true
				locks.get(pid).tids.add(tid);
				locks.put(pid, info);
				waitsForGraph.remove(tid);
				return true;
			}
		}
	}
	
	public synchronized void release(TransactionId tid, PageId pid){
			LockInfo info = locks.get(pid);
			if (info == null){
				System.err.println("Trying to release an unattained lock, no page");
				return;
			}
			if (!info.tids.contains(tid)){
				System.err.println("Trying to release an unattained lock, missing tid");
				return;
			}
			if (info.tids.size() == 1){
			  info.isExclusive = false;
			  locks.remove(pid); 
			}
			info.isExclusive = false;
			info.tids.remove(tid);
	}
	
	public boolean holdsLock(TransactionId tid, PageId pid){
		if (locks.containsKey(pid)){
			return locks.get(pid).tids.contains(tid);
		}
		return false;
	}
	
	public synchronized void releaseAll(TransactionId tid){
	    for(Entry<PageId, LockInfo> item : locks.entrySet()){
	    	if (item.getValue().tids.contains(tid)){
	    		release(tid, item.getKey());
	    	}
	    }
	}
	
	private boolean hasExclusive(TransactionId tid, PageId pid){
		if (!locks.containsKey(pid)){
			return false;
		}
		LockInfo info = locks.get(pid);
		
		return info.isExclusive && info.tids.contains(tid);
	}
	
	/*
	 * check if added tid creates deadlock, doing full cycle too inefficient
	 */
	private synchronized boolean detectDeadlock(TransactionId tid){
		Queue<TransactionId> queueDependents = new LinkedList<TransactionId>();
		Set<TransactionId> seen = new HashSet<TransactionId>();
		
		seen.add(tid);
		queueDependents.add(tid);
		while(!queueDependents.isEmpty()){
			TransactionId nextTid = queueDependents.poll();
			if(!waitsForGraph.containsKey(nextTid)){
				continue;
			}
			for(TransactionId neighbor : waitsForGraph.get(nextTid)){
				if(nextTid.equals(neighbor)){
					continue;
				}
				if(seen.contains(neighbor)){
					return true; //duel dependency, deadlock
				}
				else{
					seen.add(neighbor);
					queueDependents.add(neighbor);
				}
			}
		}	
		return false;
	}
	
	private class LockInfo{
	   public boolean isExclusive;
	   public Set<TransactionId> tids;
	   
	   public LockInfo(){
		   this.isExclusive = false;
		   this.tids = new HashSet<TransactionId>();
	   }	   
	}
	
}


