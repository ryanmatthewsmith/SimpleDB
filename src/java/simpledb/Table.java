package simpledb;

/**
 * Table stores necessary info for a table: 
 *    tableName
 *    primary key
 *    data (dbFile)
 * 
 * @author RyanMatthewSmith
 *
 */
public class Table {
	private DbFile data;
	private String tableName;
	private String primaryKey;
	
	public Table(DbFile data, String tableName, String primaryKey){
		this.data = data;
		this.tableName = tableName;
		this.primaryKey = primaryKey;

	}
	
	public TupleDesc getSchema(){
		return this.data.getTupleDesc();		
	}
	public String getTableName(){
		return this.tableName;		
	}
	public String getPrimaryKey(){
		return this.primaryKey;		
	}
	public DbFile getTableFile(){
		return this.data;		
	}
	public int getTableId(){
		return this.data.getId();
	}

}
