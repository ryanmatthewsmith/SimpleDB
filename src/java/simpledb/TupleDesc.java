package simpledb;

import java.io.Serializable;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc implements Serializable {

    /**
     * A help class to facilitate organizing the information of each field
     * */
    public static class TDItem implements Serializable {

        private static final long serialVersionUID = 1L;

        /**
         * The type of the field
         * */
        public final Type fieldType;
        
        /**
         * The name of the field
         * */
        public final String fieldName;

        public TDItem(Type t, String n) {
            this.fieldName = n;
            this.fieldType = t;
        }

        public String toString() {
            return fieldName + "(" + fieldType + ")";
        }
    }

    /**
     * @return
     *        An iterator which iterates over all the field TDItems
     *        that are included in this TupleDesc
     * */
    public Iterator<TDItem> iterator() {
        return tupleDesc.iterator();
    }

    private static final long serialVersionUID = 1L;
    private final ArrayList<TDItem> tupleDesc;
    private int size;

    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     * @param fieldAr
     *            array specifying the names of the fields. Note that names may
     *            be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
    	if (typeAr.length != fieldAr.length || typeAr.length < 1){
    	    throw new IllegalArgumentException("typeAr must contain an element, typeAr and fieldAr need to be same length");
    	}	
    	
    	tupleDesc = new ArrayList<TDItem>();
    	size = 0;    	
    	
    	for (int i = 0; i < typeAr.length; i++){
    		tupleDesc.add(new TDItem(typeAr[i],fieldAr[i]));
    		size += typeAr[i].getLen();
    	}    	       
    }

    /**
     * Constructor. Create a new tuple desc with typeAr.length fields with
     * fields of the specified types, with anonymous (unnamed) fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     */
    public TupleDesc(Type[] typeAr) {
    	if (typeAr.length < 1){
    	    throw new IllegalArgumentException("typeAr must contain an element");
    	}	
    	
    	size = 0;
        tupleDesc = new ArrayList<TDItem>();
        
        for (Type t : typeAr){
        	tupleDesc.add(new TDItem(t,""));
            size += t.getLen();	
        }
    }

    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
        return this.tupleDesc.size();
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     * 
     * @param i
     *            index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {        
    	if (i < 0 || i >= numFields()){
        	throw new NoSuchElementException();
        }
        
        return this.tupleDesc.get(i).fieldName;       
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     * 
     * @param i
     *            The index of the field to get the type of. It must be a valid
     *            index.
     * @return the type of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public Type getFieldType(int i) throws NoSuchElementException {
    	if (i < 0 || i >= numFields()){
        	throw new NoSuchElementException();
        }
        
        return this.tupleDesc.get(i).fieldType;    
    }

    /**
     * Find the index of the field with a given name.
     * 
     * @param name
     *            name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException
     *             if no field with a matching name is found.
     */
    public int fieldNameToIndex(String name) throws NoSuchElementException {
    	int elements = this.numFields();
    	
    	if (name == null) throw new NoSuchElementException();
    	
        for(int i = 0; i < elements;i++){
        	String elementFieldName = tupleDesc.get(i).fieldName;
        	if (name.equals(elementFieldName)) return i;
        }
        throw new NoSuchElementException();
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     *         Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
        return size;
    }

    /**
     * Merge two TupleDescs into one, with td1.numFields + td2.numFields fields,
     * with the first td1.numFields coming from td1 and the remaining from td2.
     * 
     * @param td1
     *            The TupleDesc with the first fields of the new TupleDesc
     * @param td2
     *            The TupleDesc with the last fields of the TupleDesc
     * @return the new TupleDesc
     */
    public static TupleDesc merge(TupleDesc td1, TupleDesc td2) {

    	int td1Elements = td1.numFields();
    	int tdElements = td1Elements+td2.numFields();
    	
        Type[] typeAr = new Type[tdElements];
        String[] fieldAr = new String[tdElements];
        
        for(int i = 0 ; i < td1Elements ; i++){
        	typeAr[i] = td1.getFieldType(i);
        	fieldAr[i] = td1.getFieldName(i);
        }
        for (int j = td1Elements; j < tdElements; j++){
        	typeAr[j] = td2.getFieldType(j-td1Elements);
        	fieldAr[j] = td2.getFieldName(j-td1Elements);
        }
        
        return new TupleDesc(typeAr, fieldAr);
    }

    /**
     * Compares the specified object with this TupleDesc for equality. Two
     * TupleDescs are considered equal if they have the same number of items
     * and if the i-th type in this TupleDesc is equal to the i-th type in o
     * for every i.
     * 
     * @param o
     *            the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */

    public boolean equals(Object o) {
        if (!(o instanceof TupleDesc)){
        	return false;
        }
        
        TupleDesc t2 = (TupleDesc)o;
        
        //num elements check
        if( this.numFields() != t2.numFields()){
        	return false;
        }
        
        //types check
        for(int i = 0; i < this.numFields(); i++){
        	if (!(this.getFieldType(i).equals(t2.getFieldType(i)))){
        		return false;
        	}
        }
        
        return true;
    }

    public int hashCode() {
        // If you want to use TupleDesc as keys for HashMap, implement this so
        // that equal objects have equals hashCode() results
        throw new UnsupportedOperationException("unimplemented");
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     * 
     * @return String describing this descriptor.
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        String divider = "";
        
        for (TDItem item : this.tupleDesc){
        	s.append(divider);
        	divider = ",";
        	s.append(item.fieldType + "(" + item.fieldName + ")");
        }
        
        return s.toString();
    }
}
