package simpledb.parallel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;

import simpledb.*;
import simpledb.OpIterator;
import simpledb.parallel.CollectProducer.WorkingThread;

/**
 * The producer part of the Shuffle Exchange operator.
 * 
 * ShuffleProducer distributes tuples to the workers according to some
 * partition function (provided as a PartitionFunction object during the
 * ShuffleProducer's instantiation).
 * 
 * */
public class ShuffleProducer extends Producer {

    private static final long serialVersionUID = 1L;
    
    private transient WorkingThread runningThread;
    
    private SocketInfo[] workers;
    private OpIterator childOpIter;
    private PartitionFunction<?, ?> pf;

    public String getName() {
        return "shuffle_p";
    }

    public ShuffleProducer(OpIterator child, ParallelOperatorID operatorID,
                           SocketInfo[] workers, PartitionFunction<?, ?> pf) {
        super(operatorID);
        this.workers = workers;
        this.childOpIter = child;
        this.pf = pf;
    }

    public void setPartitionFunction(PartitionFunction<?, ?> pf) {
        this.pf = pf;
    }

    public SocketInfo[] getWorkers() {
        return this.workers;
    }

    public PartitionFunction<?, ?> getPartitionFunction() {
        return this.pf;
    }

    // some code goes here
    class WorkingThread extends Thread {
        public void run() {
            int partitions = pf.numPartition;
            if (partitions != workers.length){
            	System.err.println("different num of workers and partitions");
            }
            
            List<IoSession> sessions = new ArrayList<IoSession>();
            List<List<Tuple>> buffers = new ArrayList<List<Tuple>>();
            Map<Integer, Long> partitionToLastTime = new HashMap<Integer, Long>();
            
            for(SocketInfo si : workers){
            	sessions.add(ParallelUtility.createSession(si.getAddress(), 
            			                                   ShuffleProducer.this.getThisWorker().minaHandler, 
            			                                   -1));
            	buffers.add(new ArrayList<Tuple>());
            }
            
            try{
            	//long lastTime  = System.currentTimeMillis();
            	
            	while(ShuffleProducer.this.childOpIter.hasNext()){
            		Tuple tup = ShuffleProducer.this.childOpIter.next();
            		int partitionForTuple = pf.partition(tup, getTupleDesc());
            		
            		List<Tuple> buffer = buffers.get(partitionForTuple);
            		IoSession session = sessions.get(partitionForTuple);
            		partitionToLastTime.put(partitionForTuple, System.currentTimeMillis());
            		
            		buffer.add(tup);
            		
            		int cnt = buffer.size();
            		if (cnt >= TupleBag.MAX_SIZE){
            			session.write(new TupleBag(
            					ShuffleProducer.this.operatorID,
            					ShuffleProducer.this.getThisWorker().workerID,
            					buffer.toArray(new Tuple[] {}),
            					ShuffleProducer.this.getTupleDesc()));
            			buffer.clear();
            			partitionToLastTime.put(partitionForTuple, System.currentTimeMillis());
            		}
            		if (cnt >= TupleBag.MIN_SIZE){
            			long thisTime = System.currentTimeMillis();
            			if (thisTime - partitionToLastTime.get(partitionForTuple) > TupleBag.MAX_MS){
            				session.write(new TupleBag(
            					ShuffleProducer.this.operatorID,
            					ShuffleProducer.this.getThisWorker().workerID,
            					buffer.toArray(new Tuple[] {}),
            					ShuffleProducer.this.getTupleDesc()));
            				buffer.clear();
            				partitionToLastTime.put(partitionForTuple, thisTime);
            			}
            		}          		
            	}
            	for(int i = 0; i < partitions; i++){
            		List<Tuple> buffer = buffers.get(i);
            		IoSession session = sessions.get(i);
            		
            		if (buffer.size() > 0){
        				session.write(new TupleBag(
            					ShuffleProducer.this.operatorID,
            					ShuffleProducer.this.getThisWorker().workerID,
            					buffer.toArray(new Tuple[] {}),
            					ShuffleProducer.this.getTupleDesc()));
        				buffer.clear();
            		}
            		session.write(new TupleBag(            					
            				ShuffleProducer.this.operatorID,
            				ShuffleProducer.this.getThisWorker().workerID)).addListener(
            						                                        new IoFutureListener<WriteFuture>(){
													                            @Override
													                            public void operationComplete(WriteFuture future) {
													                                ParallelUtility.closeSession(future.getSession());
													                            }});//.awaitUninterruptibly(); //wait until all the data have successfully transfered
            		
            	}
            }catch (DbException e){
            	System.err.println(e.getMessage());
            	e.printStackTrace();
            }catch (TransactionAbortedException e){
            	System.err.println(e.getMessage());
            	e.printStackTrace();
            }
        }
    }

    @Override
    public void open() throws DbException, TransactionAbortedException {
        this.childOpIter.open();
        this.runningThread = new WorkingThread();
        this.runningThread.start();
        super.open();
    }

    public void close() {
        super.close();
        childOpIter.close();
    }

    @Override
    public void rewind() throws DbException, TransactionAbortedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public TupleDesc getTupleDesc() {
        return this.childOpIter.getTupleDesc();
    }

    @Override
    protected Tuple fetchNext() throws DbException, TransactionAbortedException {
        try {
            // wait until the working thread terminate and return an empty tuple set
            runningThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public OpIterator[] getChildren() {
        return new OpIterator[] {this.childOpIter};
    }

    @Override
    public void setChildren(OpIterator[] children) {
    	if (this.childOpIter!=children[0])
    	{
    	    this.childOpIter = children[0];
    	}     
    }
}
