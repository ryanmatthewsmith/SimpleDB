package simpledb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;
    private final Field NO_GROUP_FIELD;
    
    private int groupByFieldIndex;
    private Type groupByFieldType;
    private int aggregateFieldIndex;
    private Op aggregateOperator;
    private Map<Field, List<Integer>> aggTable;
    private boolean noGroups;
    private boolean isSumAvg;
    private boolean isSumCount;

    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        this.groupByFieldIndex = gbfield;
        this.groupByFieldType = gbfieldtype;
        this.aggregateFieldIndex = afield;
        this.aggregateOperator = what;
        this.aggTable = new ConcurrentHashMap<Field, List<Integer>>();
        this.NO_GROUP_FIELD = new IntField(Integer.MIN_VALUE);
        this.isSumAvg = (aggregateOperator == Aggregator.Op.SC_AVG) ? true : false;
        this.isSumCount = (aggregateOperator == Aggregator.Op.SUM_COUNT) ? true : false;
        
        if (gbfield == NO_GROUPING){
        	noGroups = true;
        	aggTable.put(NO_GROUP_FIELD, new ArrayList<Integer>());
        }else{
        	noGroups = false;
        }
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
    	int value = ((IntField)tup.getField(aggregateFieldIndex)).getValue();
    	int value2 = -1;
    	if (isSumAvg){
    		value2 = ((IntField)tup.getField(aggregateFieldIndex + 1)).getValue();
    	}
    	
    	if (noGroups){
    		aggTable.get(NO_GROUP_FIELD).add(value);
    		if (isSumAvg){
    			aggTable.get(NO_GROUP_FIELD).add(value2);
    		}
    		
    	}else{
    		Field gbField = tup.getField(groupByFieldIndex);
    		if (!aggTable.containsKey(gbField)){
    			aggTable.put(gbField, new ArrayList<Integer>());
    		}
    		aggTable.get(gbField).add(value);
    		if (isSumAvg){
    			aggTable.get(gbField).add(value2);
    		}
    	}

    }
    
    /**
     *   Takes in list of values, aggregates based on Op and returns
     *   aggregate value
     */
    private Integer getAggValue(List<Integer> valueList){
    	switch(aggregateOperator){
	    	case COUNT:
	    		return valueList.size();
	    	case SUM:
	    		return valueList.stream()
                                .reduce(0, (a, b) -> a + b);
	    	case AVG:
	    		int sum = valueList.stream()
                                   .reduce(0, (a, b) -> a + b);
	    		return (sum / valueList.size());
	    	case MIN:
	    		return valueList.stream().reduce(Integer.MAX_VALUE, 
	    				                  (a, b) -> a.compareTo(b) <= 0 ? a : b);
	    	case MAX:
	    		return valueList.stream().reduce(Integer.MIN_VALUE, 
		                                  (a, b) -> a.compareTo(b) >= 0 ? a : b);
	    	case SC_AVG:
	    		int counts = 0;
	    		int sums = 0;
	    		for (int i = 0; i < valueList.size(); i++){
	    			if( (i % 2) == 0 ){
	    				counts += (valueList.get(i));
	    			}else{
	    				sums += (valueList.get(i));
	    			}
	    		}
	    		return (sums / counts);
	        //SUM_COUNT done in separate COUNT AND SUM CALLS
	    	//See iterator()
	    	default:
	    		System.err.println("missing aggregate operator");
	    		return null;
    	}
    }

    /**
     * Create a OpIterator over group aggregate results.
     * 
     * @return a OpIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public OpIterator iterator() {
    	List<Tuple> tuples = new ArrayList<Tuple>();
        if(noGroups){
        	TupleDesc td;
        	if (isSumCount){
        		td = new TupleDesc(new Type[] {Type.INT_TYPE, Type.INT_TYPE});
        	}else{
        		td = new TupleDesc(new Type[] {Type.INT_TYPE});
        	}
        	Tuple retTuple = new Tuple(td);
        	
        	if (isSumCount){
        		aggregateOperator = Aggregator.Op.COUNT;
        		int counts = this.getAggValue(aggTable.get(NO_GROUP_FIELD));
        		aggregateOperator = Aggregator.Op.SUM;
        		int sums = this.getAggValue(aggTable.get(NO_GROUP_FIELD));
        		retTuple.setField(0, new IntField(counts));
        		retTuple.setField(1, new IntField(sums));
        	}else{
        		Integer aggValue = this.getAggValue(aggTable.get(NO_GROUP_FIELD));
        		retTuple.setField(0, new IntField(aggValue));
        	}
        	tuples.add(retTuple);
        	
        	return new TupleIterator(td, tuples);
        }else{
        	TupleDesc td; 
        	
        	if (isSumCount){
        		td = new TupleDesc(new Type[]{groupByFieldType,Type.INT_TYPE,Type.INT_TYPE});
            	for (Field groupfield : aggTable.keySet()){
            		Tuple nextPair = new Tuple(td);
            		nextPair.setField(0, groupfield);
            		
            		aggregateOperator = Aggregator.Op.COUNT;
            		Integer counts = this.getAggValue(aggTable.get(groupfield));
            		aggregateOperator = Aggregator.Op.SUM;
            		Integer sums = this.getAggValue(aggTable.get(groupfield));
            		nextPair.setField(1, new IntField(counts));
            		nextPair.setField(2, new IntField(sums));
            		
            		tuples.add(nextPair);
            	}
            	
        	}else{
        		td = new TupleDesc(new Type[]{groupByFieldType,Type.INT_TYPE});
            	for (Field groupfield : aggTable.keySet()){
            		Tuple nextPair = new Tuple(td);
            		nextPair.setField(0, groupfield);
            		
            		Integer aggValue = this.getAggValue(aggTable.get(groupfield));
            		nextPair.setField(1, new IntField(aggValue));
            		
            		tuples.add(nextPair);
            	}
        	}

        	return new TupleIterator(td, tuples);     	
        }
    }

}
