package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {
	
	private File file;
	private final TupleDesc tupleDesc;
	private final int id;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        this.file = f;
        this.tupleDesc = td;
        this.id = file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere to ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        return this.tupleDesc;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
    	
    	int pageSize = BufferPool.getPageSize();
    	long offset = pid.getPageNumber() * pageSize;
    	byte[] buf = new byte[pageSize];
    	
    	try{
           RandomAccessFile raReadFile = new RandomAccessFile(this.file, "r");
           raReadFile.seek(offset);
           raReadFile.read(buf, 0, pageSize);
           raReadFile.close();
       	   
           HeapPageId Hpid = new HeapPageId(pid.getTableId(), pid.getPageNumber());
       	   return new HeapPage(Hpid, buf);
       	   
    	}catch (IOException ex){
    	   System.err.println(ex.getMessage());
    	   throw new IllegalArgumentException(); //TODO: placeholder, what to do here?
    	}    	
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        PageId pid = page.getId();
        int bufferOffset = pid.getPageNumber() * BufferPool.getPageSize();
        
        RandomAccessFile raWriteFile = new RandomAccessFile(this.file, "rw");
        raWriteFile.seek(bufferOffset);
        raWriteFile.write(page.getPageData());
        raWriteFile.close();
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        int pages = (int)Math.ceil(this.file.length() /(double)BufferPool.getPageSize());
        return pages;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
    	int numPages = this.numPages();
    	int tableId = this.id;
    	ArrayList<Page> changedPages = new ArrayList<Page>();
        
    	for (int i = 0; i < numPages ; i++){
    		HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, new HeapPageId(tableId, i), Permissions.READ_WRITE);
    		if (page.getNumEmptySlots() != 0){
    			page.insertTuple(t);
    			changedPages.add(page);
    			return changedPages;
    		}
    	}

    	//write to page first
    	writePage(new HeapPage(new HeapPageId(id, this.numPages()),HeapPage.createEmptyPageData()));
    	
    	//new page
    	HeapPage newPage = (HeapPage)Database.getBufferPool().getPage(tid, new HeapPageId(tableId,this.numPages() - 1), Permissions.READ_WRITE);
    	newPage.insertTuple(t);
    	//assert(newPage.isSlotUsed(0)) : "first slot should be taken, insertTuple in HeapFile";
    	//assert(!newPage.isSlotUsed(1)) : "second slot is used on new file, failure...HeapFile";
    	changedPages.add(newPage);
    	
    	return changedPages;   	
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
    	ArrayList<Page> changedPages = new ArrayList<Page>();
    	
        PageId pid = t.getRecordId().getPageId();
        
        if(pid.getTableId() != id){
        	throw new DbException("tuple id doesn't match heap file id");
        }
        
        HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
        page.deleteTuple(t);
        
        changedPages.add(page);
        return changedPages;
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
       return new HeapFileIterator(this, tid);
    }
}

