package simpledb;

import java.util.*;

/**
 * SeqScan is an implementation of a sequential scan access method that reads
 * each tuple of a table in no particular order (e.g., as they are laid out on
 * disk).
 */
public class SeqScan implements OpIterator {

    private static final long serialVersionUID = 1L;
    private final TransactionId transactionID;
    private int tableID;
    private String tableAlias;
    private String tableName;
    private transient DbFileIterator databaseIter;
    private TupleDesc tupleDesc;
    

    /**
     * Creates a sequential scan over the specified table as a part of the
     * specified transaction.
     *
     * @param tid
     *            The transaction this scan is running as a part of.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    public SeqScan(TransactionId tid, int tableid, String tableAlias) {
        this.transactionID = tid;
        this.reset(tableid, tableAlias);
    }
    
    /**
     * @return
     *       return the table name of the table the operator scans. This should
     *       be the actual name of the table in the catalog of the database
     * */
    public String getTableName() {
        return this.tableName;
    }

    /**
     * @return Return the alias of the table this operator scans.
     * */
    public String getAlias() {
        return this.tableAlias;
    }

    /**
     * Reset the tableid, and tableAlias of this operator.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    public void reset(int tableid, String tableAlias) {
    	this.tableID = tableid;
        this.tableAlias = tableAlias;
        
        Catalog databaseCatalog = Database.getCatalog();
        this.tableName = databaseCatalog.getTableName(tableid);
        this.databaseIter = databaseCatalog.getDatabaseFile(tableid).iterator(this.transactionID);
        this.tupleDesc = databaseCatalog.getDatabaseFile(tableid).getTupleDesc();
    }

    public SeqScan(TransactionId tid, int tableId) {
        this(tid, tableId, Database.getCatalog().getTableName(tableId));
    }

    public void open() throws DbException, TransactionAbortedException {
        this.databaseIter.open();
    }

    /**
     * Returns the TupleDesc with field names from the underlying HeapFile,
     * prefixed with the tableAlias string from the constructor. This prefix
     * becomes useful when joining tables containing a field(s) with the same
     * name.  The alias and name should be separated with a "." character
     * (e.g., "alias.fieldName").
     *
     * @return the TupleDesc with field names from the underlying HeapFile,
     *         prefixed with the tableAlias string from the constructor.
     */
    public TupleDesc getTupleDesc() {
        TupleDesc tdtemp =  this.tupleDesc;
        int numfields = tdtemp.numFields();
        
        Type[] fieldTypes = new Type[numfields];
        String[] fieldNames = new String[numfields];
        
        for (int i = 0; i < numfields ; i++){
        	fieldTypes[i] = tdtemp.getFieldType(i);
        	fieldNames[i] = this.tableAlias + "." + tdtemp.getFieldName(i);
        }
        
        return new TupleDesc(fieldTypes, fieldNames);
        
    }

    public boolean hasNext() throws TransactionAbortedException, DbException {
        return databaseIter.hasNext();
    }

    public Tuple next() throws NoSuchElementException,
            TransactionAbortedException, DbException {
        return databaseIter.next();
    }

    public void close() {
        databaseIter.close();
    }

    public void rewind() throws DbException, NoSuchElementException,
            TransactionAbortedException {
        databaseIter.rewind();
    }
}
