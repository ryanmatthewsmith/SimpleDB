package simpledb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private int groupByFieldIndex;
    private Type groupByFieldType;
    private Map<Field, Integer> aggTable;
    private boolean noGroups;
    
    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        if (what != Op.COUNT){
        	throw new IllegalArgumentException("String Types only support COUNT aggregation");
        }
        this.groupByFieldIndex = gbfield;
        this.groupByFieldType = gbfieldtype;
        this.aggTable = new ConcurrentHashMap<Field, Integer>();
        
        if (gbfield == NO_GROUPING){
        	aggTable.put(null, 0);
        	noGroups = true;
        } else{
        	noGroups = false;
        }
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
    	if (noGroups){
    		aggTable.put(null, aggTable.get(null)+1);
    	}else{
    		Field gbField = tup.getField(groupByFieldIndex);
    		if (aggTable.containsKey(gbField)){
    			aggTable.put(gbField, aggTable.get(gbField) + 1);
    		} else{
    			aggTable.put(gbField, 1);
    		}
    	}
    }

    /**
     * Create a OpIterator over group aggregate results.
     *
     * @return a OpIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */    
    public OpIterator iterator() {
    	List<Tuple> tuples = new ArrayList<Tuple>();
    	
        if (noGroups){
        	TupleDesc td = new TupleDesc(new Type[] {Type.INT_TYPE});
        	Tuple retTuple = new Tuple(td);
        	retTuple.setField(0, new IntField(aggTable.get(null)));
        	
        	tuples.add(retTuple);
        	
        	return new TupleIterator(td, tuples);
        }else{
        	TupleDesc td = new TupleDesc(new Type[]{groupByFieldType,Type.INT_TYPE});
        	
        	for (Field groupfield : aggTable.keySet()){
        		Tuple nextPair = new Tuple(td);
        		nextPair.setField(0, groupfield);
        		nextPair.setField(1, new IntField(aggTable.get(groupfield)));
        		
        		tuples.add(nextPair);
        	}
        	
        	return new TupleIterator(td, tuples);
        }
    }

}
